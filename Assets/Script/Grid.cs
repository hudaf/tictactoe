using UnityEngine;
using UnityEngine.UI;

public class Grid : MonoBehaviour
{
public SideList side;
[SerializeField] private GameManager gameManager;
[SerializeField] private Image image;
[SerializeField] private Button button;
[SerializeField] private Sprite xImage;
[SerializeField] private Sprite oImage;
    void Start()
    {
        image = gameObject.GetComponent<Image>();
        button = gameObject.GetComponent<Button>();
        side = SideList.N;
    }

    void Update()
    {
        
    }

    public void SetValue()
    {
        side = gameManager.currentSide;
        switch(side)
        {
            case SideList.X:
                image.sprite = xImage;
                break;
            case SideList.O:
                image.sprite = oImage;
                break;
        }
        button.interactable = false;
        gameManager.EndTurn();
    }
}
