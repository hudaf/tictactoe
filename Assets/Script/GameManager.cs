using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    [SerializeField] private List<Grid> grid;
    public SideList currentSide;
    public int moveCount;
    public event Action<SideList> GameWin;
    public event Action GameDraw;
    public event Action<SideList> SetSide;
    public event Action<Vector2,Vector2> SetLine;
    void Start()
    {
        currentSide = UnityEngine.Random.Range(0,2) == 0 ? SideList.X:SideList.O;
        SetSide?.Invoke(currentSide);
        moveCount = 0;
    }

    void Update()
    {
        
    }
    void ChangeSide()
    {
        currentSide = (currentSide == SideList.X) ? SideList.O : SideList.X;
        moveCount++;
        SetSide?.Invoke(currentSide);
    }
    
    void WinCheck()
    {
        if(grid[0].side == currentSide && grid[1].side == currentSide && grid[2].side == currentSide)
        {
            SetLine?.Invoke(grid[0].transform.position,grid[2].transform.position);
            EndGame(currentSide);
        }
        else if(grid[3].side == currentSide && grid[4].side == currentSide && grid[5].side == currentSide)
        {
            SetLine?.Invoke(grid[3].transform.position,grid[5].transform.position);
            EndGame(currentSide);
        }
        else if(grid[6].side == currentSide && grid[7].side == currentSide && grid[8].side == currentSide)
        {
            SetLine?.Invoke(grid[6].transform.position,grid[8].transform.position);
            EndGame(currentSide);
        }
        else if(grid[0].side == currentSide && grid[3].side == currentSide && grid[6].side == currentSide)
        {
            SetLine?.Invoke(grid[0].transform.position,grid[6].transform.position);
            EndGame(currentSide);
        }
        else if(grid[1].side == currentSide && grid[4].side == currentSide && grid[7].side == currentSide)
        {
            SetLine?.Invoke(grid[1].transform.position,grid[7].transform.position);
            EndGame(currentSide);
        }
        else if(grid[2].side == currentSide && grid[5].side == currentSide && grid[8].side == currentSide)
        {
            SetLine?.Invoke(grid[2].transform.position,grid[8].transform.position);
            EndGame(currentSide);
        }
        else if(grid[0].side == currentSide && grid[4].side == currentSide && grid[8].side == currentSide)
        {
            SetLine?.Invoke(grid[0].transform.position,grid[8].transform.position);
            EndGame(currentSide);
        }
        else if(grid[2].side == currentSide && grid[4].side == currentSide && grid[6].side == currentSide)
        {
            SetLine?.Invoke(grid[2].transform.position,grid[6].transform.position);
            EndGame(currentSide);
        }
        else if(moveCount == 8)
        {
            EndGame(SideList.N);
        }
    }
    public void EndTurn()
    {
        WinCheck();
        ChangeSide();
    }
    void EndGame(SideList currentSide)
    {
        for (int i = 0; i < grid.Count; i++)
        {
            grid[i].GetComponent<Button>().interactable = false;
        }
        if(currentSide == SideList.X || currentSide == SideList.O)
        {
            GameWin?.Invoke(currentSide);
        }
        else
        {
            GameDraw?.Invoke();
        }
    }
    void OnDestroy() 
    {
        GameWin = null;
        GameDraw = null;
        SetSide = null;
        SetLine = null;
    }
}

public enum SideList{X,O,N}
