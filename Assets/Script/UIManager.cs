using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameManager gameManager;
    [SerializeField] private TextMeshProUGUI winText;
    [SerializeField] private TextMeshProUGUI drawText;
    [SerializeField] private TextMeshProUGUI playerTurnText;
    [SerializeField] private GameObject restartButton;
    [SerializeField] private LineRenderer line;
    void Awake()
    {
        gameManager.GameWin += OnGameWin;
        gameManager.GameDraw += OnGameDraw;
        gameManager.SetSide += OnSetSide;
        gameManager.SetLine += OnSetLine;
    }
    void OnSetSide(SideList currentSide)
    {
        playerTurnText.text = "Player Turn: " + currentSide.ToString();
    }
    void OnGameWin(SideList winningSide)
    {
        winText.text = winningSide.ToString() + " Wins!";
        winText.gameObject.SetActive(true);
        restartButton.SetActive(true);
    }
    void OnSetLine(Vector2 i, Vector2 j)
    {
        line.SetPosition(0, i);
        line.SetPosition(1, j);
        line.gameObject.SetActive(true);
    }
    void OnGameDraw()
    {
        drawText.gameObject.SetActive(true);
        restartButton.SetActive(true);
    }
}
